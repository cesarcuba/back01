package com.apirest.back01.productos.Models;

public class ProductoPrecioOnly {
    private long id;
    private double precio;

    public ProductoPrecioOnly() {
    }

    public ProductoPrecioOnly(double precio) {
        this.precio = precio;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }
}
