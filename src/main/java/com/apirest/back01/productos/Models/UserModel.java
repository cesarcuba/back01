package com.apirest.back01.productos.Models;

public class UserModel {
    private String userId;

    public UserModel(){}

    public String getUserId() {
        return userId;
    }

    public UserModel(String userId) {
        this.userId = userId;
    }
}
